import java.io.*;
/**
 * @author hubert.cardot
 */
public class MLP2 {  // pg du MLP, réseau de neurones à rétropropagation

    static int classCount =3, featureCount =4, exCount =50, learningExCount =25;
    static int layerCount =3, cacheCount =6, learningCount =2000;
    static int neuronCount[]={featureCount +1, cacheCount +1, classCount}; //+1 pour neurone fixe
    static Double data[][][] = new Double[classCount][exCount][featureCount];
    static Double weight[][][], N[][], S[][], learningCoefficient =0.01, sigmoidCoefficient =2.0/3;

    private static Double fSigmoid(Double x)  {       // f()
        return Math.tanh(sigmoidCoefficient *x); }
    private static Double dfSigmoid(Double x) {       // df()
        return sigmoidCoefficient /Math.pow(Math.cosh(sigmoidCoefficient *x),2); }

    public static void main(String[] args) {
        System.out.println("Caches="+ cacheCount +" App="+ learningCount +" coef="+ learningCoefficient);
        initialisation();
        learning();
        evaluation();
    }
    private static void initialisation() {
        fileReading();
        //Allocation et initialisation aléatoire des poids
        weight = new Double[layerCount -1][][];
        for (int layer = 0; layer< layerCount -1; layer++) {
            weight[layer] = new Double[neuronCount[layer+1]][];
            for (int i = 0; i< neuronCount[layer+1]; i++) {
                weight[layer][i] = new Double[neuronCount[layer]];
                for (int j = 0; j< neuronCount[layer]; j++) {
                    weight[layer][i][j] = (Math.random()-0.5)/10; //dans [-0,05; +0,05[
                }
            }
        }
        //Allocation des états internes N et des sorties S des neurones
        N = new Double[layerCount][];
        S = new Double[layerCount][];
        for (int layer = 0; layer< layerCount; layer++) {
            N[layer] = new Double[neuronCount[layer]];
            S[layer] = new Double[neuronCount[layer]];
        }
    }
    private static void learning() {
        //---------- à faire
    }
    private static void evaluation() {
        int classFounded, ok=0, notOK=0;
        for(int i = 0; i< classCount; i++) {
            for(int j = learningExCount; j< exCount; j++) { // parcourt les ex. de test
                //---------- à faire              // calcul des N et S des neurones
                classFounded = 0;                // recherche max parmi les sorties RN
                //---------- à faire
                //System.out.println("classe "+i+" classe trouvée "+classeTrouvee);
                if (i==classFounded) ok++; else notOK++;
            }
        }
        System.out.println("Taux de reconnaissance : "+(ok*100./(ok+notOK)));
    }
    private static void propagation(Double X[]) {
        //---------- à faire
    }
    private static void retropropagation(int classe) {
        //---------- à faire
    }
    private static void fileReading() {
        // lecture des données à  partir du fichier iris.data
        String line, subString;
        int classe=0, n=0;
        try {
            BufferedReader fic=new BufferedReader(new FileReader("iris.data"));
            while ((line=fic.readLine())!=null) {
                for(int i = 0; i< featureCount; i++) {
                    subString = line.substring(i* featureCount, i* featureCount +3);
                    data[classe][n][i] = Double.parseDouble(subString);
                    //System.out.println(data[classe][n][i]+" "+classe+" "+n);
                }
                if (++n== exCount) { n=0; classe++; }
            }
        }
        catch (Exception e) { System.out.println(e.toString()); }
    }
}  //------------------fin classe MLP2--------------------